package com.file.service;

import java.util.List;
import java.util.Map;

/**
 * Created by 15319 on 2020/2/22.
 */
public interface FileService {

    List<Map> getAllChildrenFileName(String path,String orderby,boolean desc);
}
