package com.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;

@EnableDubbo
@SpringBootApplication
public class ConsumerApplication {
	//http://127.0.0.1:8081/html
	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}
}
